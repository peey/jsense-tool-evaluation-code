import in.edu.iiitd.pag.jsense.Helpers;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class TopicData {
  public static String prefix = "../../hack/jsense/data/";
  public final String topicName;
  public final String snippetsDirectory;
  public final String mappingFiles;
  public final Pattern snippetFileNamePattern;
  public final Pattern mappingFileIdentifierPattern;

  public TopicData(String topicName, String snippetsDirectory, String mappingFiles, Pattern snippetFileNamePattern, Pattern mappingFileIdentifierPattern) {
    this.topicName = topicName;
    this.snippetsDirectory =  prefix + snippetsDirectory;
    this.mappingFiles =  prefix + mappingFiles;
    this.snippetFileNamePattern = snippetFileNamePattern;
    this.mappingFileIdentifierPattern = mappingFileIdentifierPattern != null? mappingFileIdentifierPattern : snippetFileNamePattern;
  }
}

public class GoldStandard {

  //memoize because we don't expect the gold standard CSV file to change during execution
  private final HashMap<HashSet, Double> memoizedData;

  public HashMap<HashSet, Double> data() {
    return memoizedData;
  }

  public GoldStandard(TopicData td) throws IOException {
    HashMap<HashSet, Double> data = new HashMap<>();

    // INITIALIZE Gold Standard Data
    CSVFormat format = CSVFormat.DEFAULT.withDelimiter(',');

    List<File> mappingFiles = Arrays.asList(new File(td.mappingFiles).listFiles());
    Integer participants = mappingFiles.size();

    HashMap<HashSet, Double> topicData = new HashMap<>();

    for (File mappingFile : mappingFiles) {
      HashMap<Integer, Set<String>> buckets = new HashMap<>();

      for(CSVRecord record: format.parse(new FileReader(mappingFile))) {
        Integer bucketId = Integer.parseInt(record.get(1));
        buckets.putIfAbsent(bucketId, new HashSet());
        String snippetName = record.get(0);

        Matcher m = td.mappingFileIdentifierPattern.matcher(snippetName);
        if (m.matches()) {
          buckets.get(bucketId).add(snippetName);
        } else {
          System.out.printf("Warning - garbage data in gold standard mapping files. Data: %s, topic %s \n", snippetName, td.topicName);
        }
      }

      for (Integer bucketId: buckets.keySet()) {
        Set bucket = buckets.get(bucketId);
        ArrayList<String> snippetsInSameBucket = new ArrayList(bucket);

        for (int j = 0; j < snippetsInSameBucket.size(); j++) {
          for (int k = 0; k < j; k++) {
            //TODO this helpers call should be a part of the evaluation code
            String a = snippetsInSameBucket.get(j);
            String b = snippetsInSameBucket.get(k);
            HashSet hs = new HashSet(Arrays.asList(a, b));
            topicData.putIfAbsent(hs, .0);
            topicData.put(hs, topicData.get(hs) + 1);
          }
        }
      }

      for (HashSet<String> key : topicData.keySet()) {
        data.put(key, topicData.get(key)/participants);
      }
    }

    memoizedData = data;
  }

  /**
   * @param document1
   * @param document2
   * @return true if gold standard considers document1 and document2 to be similar wrt data and goldStandardCutoff
   */
  public boolean check(String document1, String document2, Double goldStandardCutoff) {
    if (document1.equals(document2)) {
      return true;
    }

    HashSet pair = new HashSet(Arrays.asList(document1, document2));

    if (data().containsKey(pair)) {
      return data().get(pair) >= goldStandardCutoff;
    } else {
      return false;
    }
  }

  /**
   * From documentCollection (which generally represents all documents in a topic),
   * pick the ones which should be in document1's result set according to goldStandard data as determined by the goldStandardCutoff
   * @param document1
   * @param documentCollection
   * @param goldStandardCutoff
   * @return A collection of documents which an ideal query would return for document1 (within documentCollection)
   * @throws Exception
   */
  public Collection<String> getRelevantDocuments(String document1, Collection<String> documentCollection, Double goldStandardCutoff) {
    Collection<String> result = new HashSet<>();
    for (String document2 : documentCollection) {
      if(!document1.equals(document2) && check(document1, document2, goldStandardCutoff)) {
        result.add(document2);
      }
    }
    return result;
  }

  /**
   * Finds avg number of matches
   * @param snippetCollection
   * @param goldStandardCutoff
   * @return
   * @throws Exception
   */
  public Pair<Double, Double> avgMatches(Collection<String> snippetCollection, Double goldStandardCutoff) {
    Integer totalMatches = 0, counter;
    List<Integer> allMatches = new ArrayList<>();

    //convert to traditional array so that we can use index-based looping. We can't use for ( element : collection ) shorthand because
    // apparently when you nest it, both are still refering to the same iterator, so both loops advance the same iterator
    ArrayList<String> snippets = new ArrayList<>(snippetCollection);
    for(int i = 0; i < snippets.size(); i++) {
      counter = 0;
      for(int j = 0; j < snippets.size() ; j++) {
        if (j != i) {
          if (check(snippets.get(i), snippets.get(j), goldStandardCutoff)) {
            counter++;
            totalMatches++;
          }
          allMatches.add(counter);
        }
      }
    }

    Collections.sort(allMatches);

    Double median;
    {
      Integer n = allMatches.size();
      Integer x = allMatches.get(n/2);
      Integer y = allMatches.get((n/2 + 1));
      if (n % 2 == 1) {
        median = (double) x;
      } else {
        median = (double) (x + y) / 2;
      }
    }

    return Pair.of((double) totalMatches/snippets.size(), median);
  }
}
