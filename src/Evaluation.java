import in.edu.iiitd.pag.jsense.CLI.API;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

class EvaluationResult {
  public final Evaluation e;
  public final Double precision;
  public final Double recall;
  public final Double avgResults;

  public EvaluationResult(Evaluation e, Double precision, Double recall, Double avgResults) {
    this.e = e;
    this.precision = precision;
    this.recall = recall;
    this.avgResults = avgResults;
  }

  @Override
  public String toString() {
    return String.format("(%-90s) \t f-score %f \t Precision %f, \t Recall %f, \t Average # of Results %f, \t Cutoff considered %f \n",
        e.evaluationDescription,
         Main.fScore(precision, recall), precision, recall,
        avgResults,
        e.scoreCutoff);
  }
}

public class Evaluation {
  public final String evaluationDescription;
  public final GoldStandard gs;
  public final Double goldStandardCutoff;
  public final Double scoreCutoff;
  public final Collection<String> documentCollection;
  public final Integer resultSize;

  public Evaluation(String evaluationDescription, GoldStandard gs, Double goldStandardCutoff, Double scoreCutoff, Collection<String> documentCollection, Integer resultSize) {
    this.evaluationDescription = evaluationDescription;
    this.gs = gs;
    this.goldStandardCutoff = goldStandardCutoff;
    this.scoreCutoff = scoreCutoff;
    this.documentCollection = documentCollection;
    this.resultSize = resultSize;
  }

  public EvaluationResult evaluate() throws Exception {
    Double totalPrecision = 0.00;
    Double totalRecall = 0.00;
    Integer totalResults = 0;
    Integer errorCases = 0;

    Double precision, recall;
    for(String s : documentCollection) {
      QueryResults qr = query(s);
      precision = getPrecisionForQueryResults(qr);
      recall = getRecallForQueryResults(qr);
      if (precision != null && recall != null) {
        totalPrecision += precision;
        totalRecall += recall;
        totalResults += qr.results.size();
      } else {
        errorCases++;
      }
    }

    Integer size = documentCollection.size() - errorCases;

    return new EvaluationResult(this, totalPrecision/size, totalRecall/size, (double) totalResults/size);
  }

  public QueryResults query(String q) throws IOException {
    PriorityQueue<Pair<String, Double>> scores = new PriorityQueue<>(Collections.reverseOrder(Comparator.comparing(p -> p.getRight())));

    for(String s: documentCollection) {
      scores.add(Pair.of(s, API.compareSnippets(s, q).CFGAllPathsScore));
    }

    ArrayList<QueryResult> matches = new ArrayList<>();

    double lastScore = 0;

    do {
      if (scores.size() == 0) {
        break;
      }

      Pair<String, Double> p = scores.poll();
      if (!p.getLeft().equals(q)) {
        matches.add(new QueryResult(q, p.getLeft(), p.getRight()));
        lastScore = p.getRight();
      }
      //System.out.printf("file: %s,\t score %d relevant %b \n", p.getValue0(), p.getValue1(), check(p.getLeft(), fileName));
      //} while (scores.size() != 0 && (scores.peek().getValue1() >= 0.005));

    } while (
        (scores.size() != 0)
            && (matches.size() < resultSize || scores.peek().getRight() == lastScore)  // result size
            && (scores.peek().getRight() >= scoreCutoff));

    return new QueryResults(q, matches);
  }

  public Double getPrecisionForQueryResults(QueryResults qr) throws Exception {
    Collection<String> relevantDocuments = gs.getRelevantDocuments(qr.query, documentCollection, goldStandardCutoff);
    if (relevantDocuments.size() == 0 || qr.results.size() == 0) {
      return null;
    } else {
      Integer relevantRetrieved = 0;
      for(QueryResult r: qr.results) {
        if(relevantDocuments.contains(r.result)) relevantRetrieved++;
      }
      return ((double) relevantRetrieved)/qr.results.size();
    }
  }

  public Double getRecallForQueryResults(QueryResults qr) throws Exception {
    Collection<String> relevantDocuments = gs.getRelevantDocuments(qr.query, documentCollection, goldStandardCutoff);
    if (relevantDocuments.size() == 0) {
      return null;
    } else {
      Integer relevantRetrieved = 0;
      for(QueryResult r: qr.results) {
        if(relevantDocuments.contains(r.result)) relevantRetrieved++;
      }

      return ((double) relevantRetrieved) / relevantDocuments.size();
    }
  }

}
