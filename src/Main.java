import in.edu.iiitd.pag.jsense.CLI.API;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
  public static Double fScore(Double precision, Double recall) {
    return precision != null && recall != null? (2 * precision * recall) / (precision + recall) : null;
  }

  public static List<TopicData> topicsData = new ArrayList<>();
  static {

    topicsData.add(new TopicData("factorial", "factorial-study", "mappings/f_mappings", Pattern.compile("1_([0-9]+).java"), null));
    //topicsData.add(new TopicData("palindrome", "palindrome-study", "mappings/pm_mappings", Pattern.compile("1_([0-9]+).txt"), Pattern.compile("1_([0-9]+).java")));
    topicsData.add(new TopicData("reverse-string", "reversestring-study", "mappings/r_mappings", Pattern.compile("rev_([0-9]+).java"), null));

    // new topics by nikita

    topicsData.add(new TopicData("binary-search", "binarysearch", "mappings/bsearch_mappings", Pattern.compile("1_([0-9]+).java"), null));
    topicsData.add(new TopicData("string-to-int", "stringtoint", "mappings/stringtoint_mappings", Pattern.compile("1_([0-9]+).java"), null));

    topicsData.add(new TopicData("read-file", "filereading", "mappings/filereading_mappings", Pattern.compile("1_([0-9]+).java"), null));
    topicsData.add(new TopicData("is-array-sorted", "isArraySorted", "mappings/is_array_sorted_mappings", Pattern.compile("1_([0-9]+).java"), null));
    topicsData.add(new TopicData("sort-array", "sortingarray", null, Pattern.compile("1_([0-9]+).java"), null));

    // new topics by nikita, added 13th July
    topicsData.add(new TopicData("fibonacci", "fibonacci", "mappings/fibonacci_mappings", Pattern.compile("1_([0-9]+).java"), null));
    topicsData.add(new TopicData("leap-year", "leapyear", null, Pattern.compile("1_([0-9]+).java"), null));
    topicsData.add(new TopicData("square-root", "squareroot", "mappings/squareroot_mappings", Pattern.compile("1_([0-9]+).java"), null));

  }

  public static void main(String[] args) throws Exception {
    TopicData t = topicsData.get(1); // factorial
    GoldStandard gs = new GoldStandard(t);

    API.initialize(t.snippetsDirectory);
    List<String> documentCollection = Arrays.stream(new File(t.snippetsDirectory).listFiles())
        .map(x -> x.getName())
        .filter(x -> {
          try {
            return API.isCompilable(x);
          } catch (IOException e) {
            e.printStackTrace();
            return false;
          }
        })
        .collect(Collectors.toList());

    Evaluation e = new Evaluation(t.topicName, gs, 0.5, 0.01, documentCollection , 10);
    EvaluationResult er = e.evaluate();
    System.out.println(er);
  }

  public static void APIUsageExample() throws IOException {
    // print score
    System.out.println(API.compareSnippets("1_56.java", "1_52.java").combinedScore);

    // check if snippet is compilable
    System.out.println(API.isCompilable("1_50.java"));

    // Complete info for debugging, optional
    System.out.println(API.dumpSnippetInfo("1_56.java"));
    System.out.println(API.dumpSnippetInfo("1_52.java"));

  }
}

