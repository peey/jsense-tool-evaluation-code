import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.util.List;

/**
 * A single result snippet. When make a query, a list of QueryResult objects is returned
 */
class QueryResult {
  String queryInfo;
  String result;
  double score;

  public QueryResult(String queryInfo, String result, double score) {
    this.queryInfo = queryInfo;
    this.result = result;
    this.score = score;
  }

  @Override
  public String toString() {
    return "QueryResult{" +  queryInfo + ", " + result + ", " + score + "}";
  }
}

public class QueryResults {
  public final String query;
  public final List<QueryResult> results;

  public QueryResults(String query, List<QueryResult> results) {
    this.query = query;
    this.results = results;
  }
}
